import math


def calc_num_segments(L, l, q) -> float:
    """Calculate the number of segments"""
    return math.log((L * (q - 1) + l) / l) / math.log(q)


def split_line_into_segments(L, l, q):
    # finding number of line segments
    num_segments = calc_num_segments(L, l, q)

    # if their number is not integer then correct l
    l = (L * (q - 1)) / ((q ** math.ceil(num_segments)) - 1)

    sum = l * ((q ** math.ceil(num_segments)) - 1) / (q - 1)
    print(sum)

    print(f"segments: {calc_num_segments(L, l, q)}\n"
          f"coefficient 'l': {l}\n"
          f"coefficient 'q': {q}")


if __name__ == '__main__':
    a = [14, 1, 2]
    split_line_into_segments(*a)
