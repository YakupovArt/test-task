import numpy as np

from unionfind import UnionFind
from itertools import product
from typing import Tuple


def component_labeling(input: np.ndarray) -> Tuple[np.ndarray, int]:
    """
    Two pass connected-component labeling, also known as the Hoshen–Kopelman algorithm, implementation.

        CCL is used to detect connected region, is an area of connected foreground pixels, in binary image.
        The algorithm makes two passes:
            1. The first pass to assign temporary labels and record equivalences;
            2. The second pass to replace each temporary label by the smallest label of its equivalence class.

        :param input: An array-like object to be labeled.
        :return: An integer ndarray where each unique feature in input has a unique label in the returned array.
    """
    input = np.asarray(input)
    output = np.empty(input.shape, np.int32)

    background = 0
    current_label = 1
    uf = UnionFind()

    # 1st pass
    for row, col in product(range(input.shape[0]), range(input.shape[1])):
        value = input[row, col]

        if value == background:
            output[row, col] = background
        else:
            labels = neighboring_labels(output, row, col)

            if not labels:
                output[row, col] = current_label
                uf.make_set(current_label)
                current_label = current_label + 1
            else:
                smallest_label = min(labels)
                output[row, col] = smallest_label

                if len(labels) > 1:
                    for label in labels:
                        uf.union(uf.get_node(smallest_label), uf.get_node(label))

    final_labels = {}
    new_label_number = 1

    # 2nd pass
    for row, col in product(range(output.shape[0]), range(output.shape[1])):
        value = output[row, col]

        if value > 0:
            new_label = uf.find(uf.get_node(value)).value
            output[row, col] = new_label
            if new_label not in final_labels:
                final_labels[new_label] = new_label_number
                new_label_number = new_label_number + 1

    for row, col in product(range(output.shape[0]), range(output.shape[1])):
        value = output[row, col]
        if value > 0:
            output[row, col] = final_labels[value]

    num_features = len(final_labels)

    return output, num_features


def neighboring_labels(input: np.ndarray, x: int, y: int) -> set:
    """
    labeling based on the connectivity and relative values of their neighbors.

        Connectivity is determined as 8-connected pixels which neighbours to
        every pixel to every pixel that touches one of their edges or corners.

        :param input: An 2d-array.
        :param x: A row.
        :param y: A column.
        :return: Gets the set of neighbouring labels.
    """
    labels = set()

    # west neighbour
    if y > 0:
        west_neighbour = input[x, y - 1]
        if west_neighbour > 0:
            labels.add(west_neighbour)

    # north neighbour
    if y > 0:
        north_neighbour = input[x - 1, y]
        if north_neighbour > 0:
            labels.add(north_neighbour)

    # north-west neighbour
    if x > 0 and y > 0:
        northwest_neighbour = input[x - 1, y - 1]
        if northwest_neighbour > 0:
            labels.add(northwest_neighbour)

    # north-east neighbour
    if x > 0 and y < len(input[x]) - 1:
        northeast_neighbour = input[x - 1, y + 1]
        if northeast_neighbour > 0:
            labels.add(northeast_neighbour)

    return labels
