import matplotlib.pyplot as plt

from polyproc import generate_features, generate_shapes, find_poly, generate_rotated_poly
from ccl import component_labeling


def visualize(space):
    plt.imshow(space, cmap="gray")
    plt.show()


def main():
    shape = (200, 200)
    figure_feat = generate_features(shape, n=15)
    space = generate_shapes(shape, figure_feat)
    ccl = component_labeling(space)
    poly = find_poly(ccl, 15)
    rotated_poly = generate_rotated_poly(shape, poly)
    visualize(space)
    visualize(rotated_poly)


if __name__ == "__main__":
    main()
