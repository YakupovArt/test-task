import numpy as np
import math

from typing import List, Tuple
from bresenham import bresenham
from rdp import approx_poly


def generate_features(grid: Tuple[int, int], n: int) -> List:
    """
    Generating the polygon features.

        Features are determined as centroid and radius of circle
        in which a polygon will be fit.

        :param grid: Shape of 2d array.
        :param n: Num of polygons to generate.
        :return: Features of each polygon.
    """
    features = []
    while n != len(features):
        r = np.random.choice(np.arange(10, 40))
        x = np.random.choice(np.arange(r, grid[0] - r))
        y = np.random.choice(np.arange(r, grid[1] - r))
        ft = np.random.choice(["triangle", "rectangle"])
        if not any(
            (x2, y2, r2)
            for x2, y2, r2, ft2 in features
            if euclidean_distance(x, y, x2, y2) < r + r2 + 5
        ):
            features.append((x, y, r, ft))

    return features


def generate_shapes(grid: Tuple[int, int], features: List) -> np.ndarray:
    """
    Generate a geometric shape.

        Each feature introduce the geometric shape. Each geometric shape points
        connects to each other by bresenham algorithm.

        :param grid: Shape of 2d array.
        :param features: Centroid, radius, type of figure.
        :return: Connected points in 2d array.
    """
    grid = np.zeros(grid)
    for feat in features:
        trp = np.round(generate_points(feat), 1).astype(np.int64)
        connect_points(grid, trp)
    return grid


def euclidean_distance(x1, y1, x2, y2):
    """Distance between points."""
    return np.hypot((x1 - x2), (y1 - y2))


def connect_points(grid: np.ndarray, points: np.ndarray) -> None:
    """
    Connect points in series using bresenham algorithm.

        :param grid: 2d array.
        :param points: Set of points.
    """
    for i in range(-1, points.shape[0] - 1):
        for x, y in bresenham(*points[i], *points[i + 1]):
            grid[x, y] = True


def generate_points(features: List) -> np.ndarray:
    """
    Process of generating points.

        Generate points on circle line such that
        connected points has geometric shape form.

        :param features: Centroid, radius, type of figure.
        :return: Set of points
    """
    if "triangle" in features:
        theta = np.array(
            [np.random.choice(np.arange(0, 1, 0.1)) * 2 * np.pi for _ in range(3)]
        )
        x = features[0] + np.cos(theta) * features[2]
        y = features[1] + np.sin(theta) * features[2]
        points = np.column_stack((x, y))
        if sum_angles(points):
            return np.round(points).astype(np.int64)
        else:
            points = generate_points(features)
    else:
        theta = (
            np.array(
                [
                    np.random.choice(np.random.random(1)),
                    np.random.choice(np.random.random(1)),
                ]
            )
            * 2
            * np.pi
        )
        rmatrix = np.array(
            [[np.cos(np.pi), -np.sin(np.pi)], [np.sin(np.pi), np.cos(np.pi)]]
        )
        xy = np.array(
            [
                features[0] + np.cos(theta) * features[2],
                features[1] + np.sin(theta) * features[2],
            ]
        ).T
        res = xy @ rmatrix.T + (2 * features[0], 2 * features[1])
        points = np.vstack((xy, res))

    return points


def sum_angles(points: np.ndarray) -> bool:
    """Verification that sum of angles equal 180 deg."""
    A = points[2] - points[0]
    B = points[1] - points[0]
    C = points[2] - points[1]

    angles = []
    for e1, e2 in ((A, B), (A, C), (B, -C)):
        num = np.dot(e1, e2)
        denom = np.linalg.norm(e1) * np.linalg.norm(e2)
        ang = np.arccos(num / denom) * 180 / np.pi
        if ang != 0:
            angles.append(ang)
    return np.round(sum(angles), 1) == 180


def rotate(p: np.ndarray, origin: np.ndarray, degrees: int) -> np.ndarray:
    """
    Rotation transform.

        :param p: Set of polygon points.
        :param origin: Centroid of polygon.
        :param degrees: Rotation angle.
        :return: Set of rotated polygon points.
    """
    angle = np.deg2rad(degrees)
    R = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
    o = np.atleast_2d(origin)
    p = np.atleast_2d(p)
    return np.squeeze((R @ (p.T - o.T) + o.T).T)


def perimeter(contour: np.ndarray) -> float:
    """
    Calculates a contour perimeter or a curve length.

        :param contour: Input vector of 2D points.
        :return: Contour perimeter.
    """
    shift_contour = np.roll(contour, 1, axis=0)
    dists = np.sqrt(np.power((contour - shift_contour), 2).sum(axis=1))
    return dists.sum()


def order_points(y1, c1, x0, c0):
    """Order points by polar angle"""
    return (math.degrees(math.atan2(y1 - c1, x0 - c0)) + 360) % 360


def shift_points(points: np.ndarray) -> np.ndarray:
    """Shift points so that beginning of set is the vertice of polygon."""
    ymin = np.min(points[:, 0])
    vertice_idx = np.max(np.where(np.isin(points[:, 0], ymin)))
    pixels = np.roll(points, -vertice_idx, axis=0)
    return pixels


def generate_rotated_poly(shape: Tuple[int, int], poly: dict) -> np.ndarray:
    """
    Generate rotated polygon.

        :param shape: Shape of 2d array.
        :param poly: Approximated polygons.
        :return: Connected and rotated points in 2d array.
    """
    grid = np.zeros(shape)
    for triangle in poly["triangles"]:
        centroid = triangle.mean(axis=0)
        points = np.round(rotate(triangle, centroid, 45), 1).astype(np.int64)
        connect_points(grid, points)
    for rectangle in poly["rectangles"]:
        connect_points(grid, rectangle)
    return grid


def find_poly(contours: Tuple[np.ndarray, int], num_poly: int) -> dict:
    """
    Determine type of polygon in 2d array.

        Approximating process to determine type of polygon.

    :param contours: Connected-component labeled 2d array.
    :param num_poly: Num of polygons.
    :return: Recognized polygons.
    """
    indices = np.indices(contours[0].shape).T[:, :, [1, 0]]
    poly = {"triangles": [], "rectangles": []}

    for i in range(1, num_poly + 1):
        pixels = indices[contours[0] == i]
        centroid = pixels.mean(axis=0)
        pixels = np.array(
            sorted(
                pixels,
                key=lambda coord: (
                    order_points(coord[1], centroid[1], coord[0], centroid[0])
                ),
            )
        )

        pixels = shift_points(pixels)
        peri = perimeter(pixels)

        pixels = np.insert(pixels, pixels.shape[0], pixels[0], axis=0)
        approximated_poly = approx_poly(pixels, epsilon=0.03 * peri)
        edges = np.unique(approximated_poly, axis=0)
        if len(edges) == 3:
            poly["triangles"].append(approximated_poly[:-1])
        else:
            poly["rectangles"].append(approximated_poly[:-1])

    return poly
