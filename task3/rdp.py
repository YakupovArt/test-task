import numpy as np


def approx_poly(points: np.ndarray, epsilon: float) -> np.ndarray:
    """
    Ramer–Douglas–Peucker algorithm implementation.

        Approximates polygon with another polygon with less vertices so that
        the distance between them is less or equal to the specified precision.

        :param points: Input vector of a 2D point.
        :param epsilon: Parameter specifying the approximation accuracy.
        :return: Approximated set of points.
    """

    # find the point with maximum distance from start to end point of set
    dmax = 0.0
    index = 0
    for i in range(1, len(points) - 1):
        d = point_line_distance(points[i], points[0], points[-1])
        if d > dmax:
            index = i
            dmax = d

    # if maximum distance is greater than epsilon then recursively simplify
    if dmax >= epsilon:
        start_idx = approx_poly(points[: index + 1], epsilon)[:-1]
        end_idx = approx_poly(points[index:], epsilon)
        results = np.vstack((start_idx, end_idx))
    else:
        results = np.vstack((points[0], points[-1]))

    return results


def point_line_distance(point: np.ndarray, start: np.ndarray, end: np.ndarray) -> float:
    """
    Calculates the distance from point to the line given by start and end.

        :param point: The point that is farthest from the line segment with start and end points.
        :param start: The first point of the given set.
        :param end: The last point of the given set.
        :return: The distance from point to the line.
    """
    if np.array_equal(start, end):
        return np.linalg.norm(point - start)
    else:
        return np.divide(
            np.abs((np.cross(end - start, start - point))),
            np.linalg.norm(end - start),
        )
