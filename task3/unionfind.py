class Node(object):
    """Node class for data structure with Union-Find algorithm."""
    def __init__(self, value):
        self.value = value
        self.parent = self
        self.rank = 0


class UnionFind:
    """
    Disjoint-set, also known as the union-find, is a data structure
    that stores a collection of disjoint (non-overlapping) sets.
    """
    def __init__(self):
        self._nodes_addressed_by_value = {}

    def make_set(self, value: int) -> Node:
        """
        Adds a new element.

            This element is placed into a new set containing only the new element
            and the new set is added to the data structure.

            :param value: A new element.
            :return: A new set of containing one node with value
        """
        if self.get_node(value):
            return self.get_node(value)

        node = Node(value)
        self._nodes_addressed_by_value[value] = node
        return node

    def find(self, x: Node) -> Node:
        """
        Determines which subset a particular element is in.

            The operation follows the chain of parent pointers from
            a specified query node until it reaches a root element.

            :param x: A valid node id.
            :return: The root node id of the connected component 'x'.
        """
        if x.parent != x:
            x.parent = self.find(x.parent)
        return x.parent

    def union(self, x: Node, y: Node):
        """
        Replaces the set containing 'x' and the set containing 'y' with their union.

            Union first uses find operation to determine the roots of trees containing x and y.
            If the roots are the same, there is nothing more to do. Otherwise, the two trees must be merged.

            :param x: A valid node id.
            :param y: A valid node id.
        """
        if x == y:
            return

        x_root = self.find(x)
        y_root = self.find(y)

        if x_root == y_root:
            return

        if x_root.rank > y_root.rank:
            y_root.parent = x_root
        elif x_root.rank < y_root.rank:
            x_root.parent = y_root
        else:
            x_root.parent = y_root
            y_root.rank = y_root.rank + 1

    def get_node(self, value: int):
        """
        Returns a node of disjoint set.

            :param value: A element.
            :return: Node with value.
        """
        if value in self._nodes_addressed_by_value:
            return self._nodes_addressed_by_value[value]
        else:
            return False
